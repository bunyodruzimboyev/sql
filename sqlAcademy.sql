####
1. 
SELECT name
FROM Passenger;

####
2.
SELECT name
FROM Company;

####
3. 
SELECT *
FROM Trip
WHERE town_from = 'Moscow';

####
4.
SELECT name
FROM Passenger
WHERE name LIKE '%man';

####
5.
SELECT COUNT(*) as count
FROM Trip
WHERE plane = 'TU-134';

####
6.
SELECT DISTINCT name 
FROM Company as c
    JOIN Trip as t ON c.id = t.company
WHERE t.plane = 'Boeing';
  
####
7. 
SELECT DISTINCT plane
FROM Trip
WHERE town_to = 'Moscow';

####
8. 
SELECT town_to, TIMEDIFF(time_in, time_out) as flight_time
FROM Trip
WHERE town_from = 'Paris';

####
9.
SELECT DISTINCT name 
FROM Company c
    JOIN Trip t ON c.id  = t.company 
WHERE t.town_to = 'Vladivostok' OR t.town_from = 'Vladivostok';

####
10.
SELECT DISTINCT *
FROM Trip
WHERE DATE_FORMAT(time_out, '%d-%m-%Y %H:%i:%s') >= '01-01-1900 10:00:00' AND 
      DATE_FORMAT(time_out, '%d-%m-%Y %H:%i:%s') <= '01-01-1900 14:00:00';
	  
####
11.
SELECT name
FROM Passenger
WHERE LENGTH(name) = (SELECT MAX(LENGTH(name)) FROM Passenger);

####
12.
SELECT trip, COUNT(Passenger) as count
FROM Pass_in_trip
GROUP BY Trip;

####
13.
SELECT  DISTINCT name
FROM Passenger
GROUP BY name
HAVING COUNT(name) > 1;

####
14.
SELECT town_to 
FROM Trip t
    JOIN Pass_in_trip pt ON t.id = pt.trip
    JOIN Passenger p ON pt.passenger = p.id
WHERE p.name = 'Bruce Willis';

####
15.
SELECT DISTINCT time_in
FROM Trip t 
    JOIN Pass_in_trip pt ON t.id = pt.trip
    JOIN Passenger p ON pt.passenger = p.id
WHERE p.name = 'Steve Martin' AND t.town_to = 'London';

####
16.
SELECT COUNT(pt.trip) as count, p.name 
FROM Passenger p
JOIN Pass_in_trip pt ON p.id = pt.passenger
GROUP BY p.name
ORDER BY 1 DESC, p.name ASC;

####
17.
SELECT fm.member_name,
  fm.status,
  sum(amount * unit_price) as costs
FROM Payments p
  JOIN FamilyMembers fm on fm.member_id = p.family_member
WHERE YEAR(p.date) = 2005
GROUP by family_member

####
18.
SELECT member_name
FROM FamilyMembers
ORDER BY birthday ASC
LIMIT 1;

####
19.
SELECT DISTINCT status
FROM FamilyMembers
  JOIN Payments ON FamilyMembers.member_id = Payments.family_member
  JOIN Goods ON Payments.good = Goods.good_id
WHERE good_name = 'potato';

####
20.
SELECT fm.status, fm.member_name, SUM(amount * unit_price) as costs
FROM Payments p 
JOIN FamilyMembers fm ON p.family_member = fm.member_id
JOIN Goods g ON p.good = g.good_id
JOIN GoodTypes gt ON g.type = gt.good_type_id
WHERE gt.good_type_name = 'entertainment'
GROUP BY family_member;

####
21.
SELECT good_name 
FROM  Goods
JOIN Payments ON Goods.good_id = Payments.good 
GROUP BY good_name 
HAVING COUNT(good) > 1;

####
22.
SELECT DISTINCT member_name
FROM FamilyMembers
WHERE status = 'mother';

####
23.
SELECT good_name, unit_price 
FROM Goods
JOIN Payments ON Goods.good_id = Payments.good 
JOIN GoodTypes ON Goods.type  = GoodTypes.good_type_id 
WHERE good_type_name = 'delicacies'
ORDER  by  unit_price DESC LIMIT 1;

####
24.
SELECT member_name, SUM(amount * unit_price) as costs
FROM Payments
JOIN FamilyMembers ON Payments.family_member = FamilyMembers.member_id 
WHERE YEAR(date) = 2005 AND MONTH(date) = 6
GROUP BY member_name;

####
25.
SELECT good_name 
FROM Goods
WHERE good_id NOT IN (
    SELECT good 
    FROM Payments 
    WHERE YEAR(date) = 2005 
);

####
26.
SELECT DISTINCT good_type_name
FROM GoodTypes
WHERE good_type_id NOT IN (
    SELECT good_type_id FROM GoodTypes, Goods, Payments
    WHERE good_type_id = type AND good_id = good AND YEAR(date) = 2005
  );

####
27.
SELECT good_type_name, SUM(amount * unit_price) as costs
FROM Payments
JOIN Goods ON Payments.good = Goods.good_id 
JOIN GoodTypes ON Goods.type =GoodTypes.good_type_id 
WHERE YEAR(date) = 2005
GROUP BY good_type_name;


####
28.
SELECT COUNT(*) as count
FROM Trip
WHERE town_from = 'Rostov'
  AND town_to = 'Moscow';

####
29.
SELECT DISTINCT p.name 
FROM Passenger p 
JOIN Pass_in_trip pt ON p.id = pt.passenger
JOIN Trip t ON pt.trip = t.id
WHERE t.plane = 'TU-134' AND t.town_to = 'Moscow'; 

####
30.
SELECT trip, COUNT(passenger) as count
FROM Pass_in_trip
GROUP BY trip 
ORDER BY  COUNT(passenger) DESC  ;

####
31.
SELECT DISTINCT *
FROM FamilyMembers
WHERE member_name LIKE "%Quincey";

####
32.
SELECT ROUND(AVG(YEAR(CURDATE()) - YEAR(birthday)), 0) AS age
FROM FamilyMembers;

####
33.
SELECT DISTINCT AVG(Payments.unit_price) AS cost
FROM Payments
JOIN Goods ON Payments.good = Goods.good_id 
WHERE Goods.good_name like "%caviar";

####
34.
SELECT COUNT(*) as count
FROM Class
WHERE name like '10 %';

####
35.
SELECT COUNT(classroom) AS count
FROM Schedule
WHERE YEAR(date) = 2019 AND MONTH(date) = 9 AND DAY(date) = 2;

####
36.
SELECT 
FROM Student
WHERE address LIKE 'ul. Pushkina%';

####
37.
1-si xato yul
SELECT YEAR(CURDATE()) - YEAR(birthday) - 1 AS year
FROM Student
WHERE birthday = (SELECT MAX(birthday)
                  FROM Student);
                  
SELECT TIMESTAMPDIFF(YEAR, birthday, date(CURDATE())) AS year
FROM Student
WHERE birthday = (SELECT MAX(birthday)
                  FROM Student);

####
38.
SELECT COUNT(first_name) AS count
FROM Student
WHERE first_name LIKE "Ann%";

####
39.
SELECT COUNT(*) AS count
FROM Student
  JOIN Student_in_class on Student.id = Student_in_class.student
  JOIN Class on Student_in_class.class = Class.id
WHERE Class.name = '10 B';

####
40.
SELECT Subject.name AS subjects
FROM Teacher, Subject, Schedule
WHERE   Schedule.teacher = Teacher.id AND Schedule.subject = Subject.id AND 
        last_name = 'Romashkin' AND middle_name LIKE "P%" AND first_name LIKE "P%";

####
41.
SELECT DISTINCT start_pair 
FROM Schedule
JOIN Timepair ON Schedule.number_pair = Timepair.id
ORDER BY start_pair ASC LIMIT 1 OFFSET 3; 

####
42.
SELECT TIMEDIFF(
    (
      SELECT DISTINCT end_pair
      FROM Schedule
        JOIN Timepair ON Schedule.number_pair = Timepair.id
      ORDER BY end_pair ASC
      LIMIT 1 OFFSET 3
    ),
(
      SELECT DISTINCT start_pair
      FROM Schedule
        JOIN Timepair ON Schedule.number_pair = Timepair.id
      ORDER BY start_pair ASC
      LIMIT 1 OFFSET 1
    )
  ) AS time;

####
43.
SELECT last_name 
FROM Teacher, Subject, Schedule
WHERE Subject.name = 'Physical Culture' AND 
      Schedule.teacher = Teacher.id AND 
      Schedule.subject = Subject.id
ORDER BY last_name ASC;

####
44.
SELECT MAX(TIMESTAMPDIFF(YEAR, birthday, CURRENT_DATE)) AS max_year
FROM Student, Student_in_class, Class
WHERE Student_in_class.class = Class.id AND 
      Student_in_class.student = Student.id AND 
      Class.name LIKE "10%";

####
45.
SELECT DISTINCT classroom 
FROM Schedule s1
WHERE (SELECT COUNT(classroom )
       FROM Schedule
       GROUP BY classroom 
       HAVING classroom = s1.classroom) 
       = 
       (SELECT COUNT(classroom) c
       FROM Schedule
       GROUP BY classroom 
       ORDER BY c DESC LIMIT 1
       );

####
46.
SELECT DISTINCT name  
FROM Class, Teacher, Schedule
WHERE Class.id = Schedule.class AND Teacher.id = Schedule.teacher AND 
      last_name LIKE "Krauze%";

####
47.
SELECT DISTINCT name  
FROM Class, Teacher, Schedule
WHERE Class.id = Schedule.class AND Teacher.id = Schedule.teacher AND 
      last_name LIKE "Krauze%";

####
48.
SELECT name, COUNT(student) AS count
FROM Class, Student_in_class
WHERE Class.id = Student_in_class.class
GROUP BY name 
ORDER BY COUNT(student) DESC;

####
49.
SELECT (SELECT COUNT(student)
        FROM Class, Student_in_class
        WHERE Student_in_class.class = Class.id AND Class.name = '10 A')
        /
        (SELECT COUNT(student)
        FROM Student_in_class)
        * 100
        AS percent;

####
50.
SELECT ROUND((SELECT COUNT(student)
             FROM Class, Student_in_class, Student
             WHERE Student_in_class.class = Class.id AND 
                   Student_in_class.student  = Student.id AND 
                   YEAR(Student.birthday) = 2000)
             /
             (SELECT COUNT(student)
             FROM Student_in_class)
             * 100, 0)
        AS percent;

####
51.
INSERT INTO Goods 
        SET good_id = (SELECT COUNT(*) + 1 FROM Goods as Ixtiyoriy_Nom),
            good_name ='Cheese', 
            type = (SELECT good_type_id 
                    FROM GoodTypes
                    WHERE good_type_name = 'food'); 

####
52.
INSERT INTO GoodTypes 
        SET good_type_id = (SELECT MAX(good_type_id) + 1 FROM GoodTypes AS GT),
            good_type_name = 'auto';

####
53.
UPDATE FamilyMembers
SET member_name = 'Andie Anthony'
WHERE member_name = 'Andie Quincey';

####
54.
DELETE FROM FamilyMembers
WHERE member_name LIKE '%Quincey';

####
55.
DELETE FROM Company AS c1
WHERE (SELECT COUNT(company)
       FROM Trip
       GROUP BY company  
       HAVING company = c1.id) 
       = 
       (SELECT COUNT(company) c
       FROM Trip
       GROUP BY company  
       ORDER BY c ASC LIMIT 1
       );

####
56.
DELETE FROM Trip
WHERE town_from = 'Moscow';

####
57.
UPDATE Timepair SET  
start_pair  = start_pair  + interval 30 MINUTE,
end_pair = end_pair + interval 30 MINUTE;

####
58.
INSERT INTO Reviews
SET id = (
    SELECT MAX(id) + 1
    FROM Reviews AS r2
  ),
  reservation_id = (
    SELECT DISTINCT rev.id
    FROM Reservations rev
      JOIN Users u ON rev.user_id = u.id
      JOIN Rooms r ON rev.room_id = r.id
    WHERE u.name = 'George Clooney'
      AND r.address = '11218, Friel Place, New York'
  ),
  rating = 5;

####
59.
SELECT * 
FROM Users
WHERE phone_number LIKE "+375%";
      
####
60.
SELECT teacher 
FROM Schedule
JOIN Class ON Class.id = Schedule.class 
WHERE name LIKE "11%"
GROUP BY teacher 
HAVING COUNT(DISTINCT name) = (SELECT COUNT(name)
                      FROM Class  
                      WHERE name like "11%");

####
61.
SELECT DISTINCT Rooms.*
FROM Rooms
JOIN Reservations ON Reservations.room_id = Rooms.id
WHERE YEAR(start_date) = 2020 AND WEEKOFYEAR(start_date) = 12;

####
62.
SELECT SUBSTRING(email, POSITION('@' IN email) + 1) as domain, COUNT(SUBSTRING(email, POSITION('@' IN email) + 1)) as count
FROM Users
GROUP BY 1
ORDER BY COUNT(1) DESC, 1 ASC;

####
63.
SELECT DISTINCT CONCAT(last_name, '.', SUBSTRING(first_name, 1,1), '.', SUBSTRING(middle_name, 1, 1), '.') as name 
FROM Student
ORDER BY name ASC;

####
64.


####
65.
SELECT  room_id, FLOOR(AVG(rating)) AS rating  
FROM  Reservations
JOIN Reviews ON Reservations.id = Reviews.reservation_id 
GROUP BY room_id 
ORDER BY 2 DESC;

####
66.
SELECT home_type, address, 
       IFNULL(SUM(TIMESTAMPDIFF(DAY,start_date, end_date )),0) as days,
       IFNULL(SUM(total),0) as total_fee
FROM Rooms
LEFT JOIN Reservations ON Rooms.id = Reservations.room_id 
WHERE has_kitchen = 1 AND has_air_con = 1 AND has_internet = 1 AND has_tv =1
GROUP BY Rooms.id;


####
67.
SELECT DISTINCT CONCAT(DATE_FORMAT(time_out, "%H:%i, %e.%c"), ' - ',DATE_FORMAT(time_in , "%H:%i, %e.%c"))
 as flight_time
FROM Trip;

####
68.

####
69.

####
70.
SELECT "economy" as category, COUNT(price) as count
FROM Rooms
WHERE price <= 100
GROUP BY 1
UNION 
SELECT "comfort" as category, COUNT(price) as count
FROM Rooms
WHERE price > 100 AND price < 200
GROUP BY 1
UNION 
SELECT "premium" as category, COUNT(price) as count
FROM Rooms
WHERE price >= 200
GROUP BY 1;

####
71.
